require "lib_gpio/version"
require "rb-inotify"

module LibGPIO

  class Pin

    @@notifier = INotify::Notifier.new

    def initialize(number, options = {})
      unless Dir.exists? gpio_path(number)
        File.write File.join(gpio_path, 'export'), number
      end
      @number = number

      options.each do |option, value|
        case option
        when :direction
          self.direction = value
        when :value
          self.value = value
        end
      end
    end

    def direction=(direction)
      File.write(File.join(gpio_path(@number), 'direction'), direction)
    end

    def direction
      File.read(File.join(gpio_path(@number), 'direction')).to_sym
    end

    def value=(value)
      if self.direction == :in
        raise "Cannot set value on input pins. Change pin direction first."
      end
      File.write(File.join(gpio_path(@number), 'value'), value)
      value
    end

    def value
      File.read(File.join(gpio_path(@number), 'value')).to_i
    end

    def toggle!
      self.value = self.value == 0 ? 1 : 0
    end

    def watch
      @@notifier.watch(File.join(gpio_path(@number), 'value'), :modify) do
        yield self.value
      end
    end

    private

    def gpio_path(pin_number = nil)
      "/sys/class/gpio#{"/gpio#{pin_number}" if pin_number}"
    end
  end

end
